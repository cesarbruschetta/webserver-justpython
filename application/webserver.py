"""
Very simple HTTP server in python (Updated for Python 3.7)
"""
import re
import os
import json
from string import Template
from http.server import HTTPServer, BaseHTTPRequestHandler


BASE_PATH = os.path.dirname(os.path.abspath(__file__))
DATABASE = [{"name": "Cesar Augusto"}]


class BaseHandler(BaseHTTPRequestHandler):

    content_type = "text/html"
    content_data = ""
    content_code = 200

    def do_GET(self):
        self._process_request()
        self._set_headers()
        self.wfile.write(self.content_data.encode("utf-8"))

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        # Doesn't do anything with posted data
        self._process_request()
        self._set_headers()
        self.wfile.write(self.content_data.encode("utf-8"))

    def _set_headers(self):
        self.send_response(self.content_code)
        self.send_header("Content-type", self.content_type)
        self.end_headers()

    def _process_request(self):
        if re.match(r"^\/?$", self.path):
            self.website_responses()
        elif re.match(r"^/form\/?", self.path):
            self.form_responses()
        elif re.match(r"^/static/", self.path):
            self.static_responses()
        elif re.match(r"^/api/?", self.path):
            self.api_responses()
        else:
            self.not_found_responses()

    def not_found_responses(self):
        self.content_code = 404
        self.send_error(404, "Not Found: %s" % self.path)

    def static_responses(self):
        if self.path.endswith(".jpg"):
            mimetype = "image/jpg"
        if self.path.endswith(".gif"):
            mimetype = "image/gif"
        if self.path.endswith(".js"):
            mimetype = "application/javascript"
        if self.path.endswith(".css"):
            mimetype = "text/css"
        else:
            mimetype = "text/plan"
        try:
            with open(BASE_PATH + self.path) as f:
                data = f.read()

            self.content_code = 200
            self.content_type = mimetype
            self.content_data = data

        except (FileNotFoundError, IOError):
            self.content_code = 404
            self.send_error(404, "File Not Found: %s" % self.path)

    def api_responses(self):
        self.content_code = 200
        self.content_type = "application/json"
        if self.command == "POST":
            length = int(self.headers.get("content-length", "0"))
            data = json.loads(self.rfile.read(length))
            if data.get("name"):
                DATABASE.append(data)
                self.content_data = json.dumps({"status": "ok"})
            else:
                self.content_code = 400
                self.send_error(400, "Error in payload: %s" % data)
        else:
            self.content_data = json.dumps(DATABASE)

    def website_responses(self):
        rendered = Template(open(BASE_PATH + "/templates/base.html").read())
        data = {
            "data_name": "".join(
                [
                    f"<tr><td>{i}</td><td>{item['name']}</td></tr>"
                    for i, item in enumerate(DATABASE)
                ]
            )
        }

        self.content_data = rendered.substitute(data)

    def form_responses(self):
        rendered = Template(open(BASE_PATH + "/templates/form.html").read())
        self.content_data = rendered.substitute({})
