# WebServer - JustPython 3.0

## Chega de Framework web, vamos fazer uma aplicação web só com Python

Olá, Essa aplicação web utilizando somente Python, criando  assim uma api rest e renderizando um site com HTML e CSS.
Apresentando dessa forma a gama de ferramentas built-in da biblioteca Python e mostrando assim a versatilidade e funcionalidade como uma linguagem multi-plataforma.

### Instalação



### Utilização

`python application/app.py [-h] [-l LISTEN] [-p PORT]`

optional arguments:

  * `-h, --help` show this help message and exit
  * `-l LISTEN, --listen LISTEN` Expecifique o IP que o servidor ira escutar
  * `-p PORT, --port PORT` Expecifique a porta que o servidor ira escutar

```shell
$  python application/app.py -l 0.0.0.0 -p 8080
servidor web rodando no endereço http://0.0.0.0:8080/
```

### Teste de desempenho
 
 `./scripts/benchmark.sh <numero de requests> <requests concorentes>`

```shell
$ ./scripts/benchmark.sh 10 10
```

### Autor

Cesar Augusto

* [GitHub](https://github.com/cesarbruschetta)
* [Linkedin](https://www.linkedin.com/in/cesarbruschetta/)
